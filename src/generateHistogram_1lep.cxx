#include "generateHistogram_1lep.h"
#include "histSvc.h"
#include "ReadTruthTree.h"
#include "MVATree.h"

#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TKey.h>
#include <TList.h>
#include <TClass.h>
#include <TRegexp.h>
#include "TChain.h"
#include "TLorentzVector.h"

#include <iostream>
#include <fstream>
#include <vector>

generateHistogram_1lep::generateHistogram_1lep() {

}

generateHistogram_1lep::~generateHistogram_1lep() {

}

void generateHistogram_1lep::FillHistograms(TString DSID){
  Selection1Lepton(DSID);  //Needed to steer the code to run the 1-lepton selection
}

void generateHistogram_1lep::Selection1Lepton(TString DSID){

  int nEvents = ( m_nEvents == -1 ) ? m_inputChain->GetEntries() : m_nEvents;  // Reading in the number of events, and if set to run on a subset
  float m_weight = 1;

  int tenPercent = nEvents / 5;
  
  //TFile*FF_El = new TFile("data/3DFF_El.root");
  //TH3D*h3d_El = (TH3D*)FF_El->Get("FF_El");
  TFile*FF_El = new TFile("data/FF_El.root");
  TH2D*h2d_El = (TH2D*)FF_El->Get("FF_El");
  TFile*FF_Mu = new TFile("data/FF_Mu.root");
  TH2D*h2d_Mu = (TH2D*)FF_Mu->Get("FF_Mu");

    for( int i=0; i<nEvents; ++i ){
    //gDebug=2;
	readNtuple->GetEntry(i);  // Load the event
	m_weight = readNtuple->EventWeight;
    if(tenPercent>0){
    if( i%tenPercent == 0 ) std::cout << " Running on event " << i << " of " << nEvents << std::endl;
	}
	float MCWeight = readNtuple->MCWeight;
	unsigned long long RunNumber = readNtuple->RunNumber;
	unsigned long long EventNumber = readNtuple->EventNumber;
	float mBB = readNtuple->jjM;
	float MET = readNtuple->met;
	float pTV = readNtuple->WPt;
    float Mtop = readNtuple->Mtop;
    float mTW  = readNtuple->WM;
	float dYWH  = readNtuple->dYWH;
	float dRBB  = readNtuple->jjdR;
	float mBBJ  = readNtuple->jjjM;
	float mindPhiLJ  = readNtuple->ljmindPhi;
	float dPhiVBB  = readNtuple->WjjdPhi;
	float Ptl  = readNtuple->lPt;
	float Etal  = readNtuple->lEta;
	float Phil  = readNtuple->lPhi;
    float PtB1  = readNtuple->j1Pt;
    float EtaB1  = readNtuple->j1Eta;
    float PhiB1  = readNtuple->j1Phi;
    float PtB2  = readNtuple->j2Pt;
    float EtaB2  = readNtuple->j2Eta;
    float PhiB2  = readNtuple->j2Phi;
    float PtJ3  = readNtuple->j3Pt;
    float EtaJ3  = readNtuple->j3Eta;
    float PhiJ3  = readNtuple->j3Phi;
	float mu = readNtuple->mu;
	float AverageMuScaled = readNtuple->AverageMuScaled;
	float AverageMu = readNtuple->AverageMu;
	float ActualMuScaled = readNtuple->ActualMuScaled;
	float ActualMu = readNtuple->ActualMu;
    int nTags = readNtuple->nTags;
	int nSJ = readNtuple->nSJ;
	int nFJ = readNtuple->nFJ;
	int nJets = readNtuple->nJets;
	int nTaus = readNtuple->nTaus;
    int j1Flav = readNtuple->j1Flav;
	int j2Flav = readNtuple->j2Flav;
	int nElectrons = readNtuple->nElectrons; 
	float mva = readNtuple->BDT;
	float mvadiboson = readNtuple->BDTWZ;
    float mva_21 = readNtuple->BDT_21;
	float mvadiboson_21 = readNtuple->BDTWZ_21;
	float lmetdPhi = readNtuple->lmetdPhi;
	float jmetdPhi = readNtuple->jmetdPhi;
	float jldPhi = readNtuple->jldPhi;
	//float jjmindPhi = readNtuple->jjmindPhi;
	float topoetcone20 = readNtuple->topoetcone20;
	float ptcone20 = readNtuple->ptcone20;
	//std::cout<<"topoetcone20=="<<topoetcone20<<"\n";
	//std::cout<<"ptcone20=="<<ptcone20<<"\n";
	float d0sigBL = readNtuple->d0sigBL;
	float z0sinTheta = readNtuple->z0sinTheta;

    float jjmetdPhi = readNtuple->jjmetdPhi;
	float jjjmetdPhi =  readNtuple->jjjmetdPhi;
	float jjldPhi = readNtuple->jjldPhi;
	float jjjldPhi = readNtuple->jjjldPhi;
    //Yanhui
	
	vector<float>* SJet_Pt       = readNtuple->SJet_Pt;
	vector<float>* SJet_Eta      = readNtuple->SJet_Eta;
	vector<bool>*  SJet_isTagged = readNtuple->SJet_isTagged;
	vector<float>* SJet_Jvt      = readNtuple->SJet_Jvt;
    vector<bool>*  FJet_JvtTight = readNtuple->FJet_JvtTight;
	vector<float>* FJet_Pt       = readNtuple->FJet_Pt;
    vector<int>*   SJet_PartonTruthLabelID = readNtuple->SJet_PartonTruthLabelID;
	vector<int>*   FJet_PartonTruthLabelID = readNtuple->FJet_PartonTruthLabelID;
	//Ht Ratio Calculation
    /*	
	float Ht=0;
	float HtB=0;
	for(int ii=0;ii<SJet_Pt->size();ii++){
	 Ht+=SJet_Pt->at(ii);
	 if(SJet_isTagged->at(ii)==1){
	 HtB+=SJet_Pt->at(ii);
	 }
	}
    for(int i=0;i<FJet_Pt->size();i++){
	 Ht+=FJet_Pt->at(i);
	}
	float HtRatio = HtB / Ht;
    */
    /*
	bool PUJets = false;
    for(int ii=0;ii<SJet_PartonTruthLabelID->size();ii++){
	 if(SJet_PartonTruthLabelID->at(ii)==-1) PUJets = true;
	}
	*/
	/*
	for(int i=0;i<FJet_PartonTruthLabelID->size();i++){
	 if(FJet_PartonTruthLabelID->at(i)==-1) PUJets = true;
	}
	*/
	/*
    for(int ii=0;ii<SJet_Pt->size();ii++){
	 //if(SJet_isTagged->at(ii)==0){//no b
	  if( fabs(SJet_Eta->at(ii))>2.4 ){ //2.5>|Eta|>2.4
	  if(SJet_Pt->at(ii)<30e3){//pt>30GeV 
	   nSJ-=1;
	   if(SJet_isTagged->at(ii)==1){
	    nTags-=1;	
	  }
	 }
	}
   //}
	}
	*/
    /* 
	for(int ii=0;ii<SJet_Pt->size();ii++){
     if(SJet_isTagged->at(ii)==0){//no b
      if(SJet_Pt->at(ii)<60e3){//pt<60GeV
       if(fabs(SJet_Eta->at(ii))<2.4){//|eta|<2.4
        if(SJet_Jvt->at(ii)<0.91){//tight jvt cut
		//std::cout<<"Jvt == "<<SJet_Jvt->at(ii)<<"\n"; 
        nSJ-=1;
		if(SJet_isTagged->at(ii)==1) 
		 {
		 //std::cout<<" a b jet not pass the tight JVT cut "<<"\n";
		 nTags-=1;
		 }
        }
       }
      }
     }
    }
	*/
	/*
    for(int ii=0;ii<FJet_Pt->size();ii++){
      if(FJet_JvtTight->at(ii)==0){
       nFJ-=1;
      }
      else if(FJet_PartonTruthLabelID->at(ii)==-1){
        PUJets=true;
      }
     }
    */
 	/*
	//std::cout<<"NFJ == "<<FJet_JvtTight->size()<<"\n";
    for(auto itr = FJet_JvtTight->begin();itr != FJet_JvtTight->end();){
     if(!*itr) {
      //std::cout<<"FJet_JvtTight == "<<*itr<<"\n";
      FJet_JvtTight->erase(itr);
     }
      else ++itr; 
    }
    */
    //std::cout<<"NFJ == "<<FJet_JvtTight->size()<<"\n";
    //nFJ = FJet_JvtTight->size(); 
	
    //nJets = (nSJ+nFJ); //Keep It!!!!!!

	m_histFill->SetFlavor(j1Flav,j2Flav);
    if( m_DSID.Contains("Wenu") || m_DSID.Contains("Wmunu") || m_DSID.Contains("Wtaunu") || m_DSID.Contains("Zee") || m_DSID.Contains("Zmumu") || m_DSID.Contains("Znunu") || m_DSID.Contains("Ztautau") ) {
	 if(i==0) std::cout<<"DSID == "<<m_DSID<<"\n";

	if(MCWeight>=100 || MCWeight<=-100){
    std::cout<<"MCWeight =="<<MCWeight<<"\n";
	std::cout<<"RunNumber =="<<RunNumber<<"\n";
    std::cout<<"EventNumber =="<<EventNumber<<"\n";
	std::cout<<"EventWeight =="<<m_weight<<"\n";
	std::cout<<"nElectrons =="<<nElectrons<<"\n";
	std::cout<<"pTV =="<<pTV<<"\n";
	std::cout<<"mBB =="<<mBB<<"\n";
	std::cout<<"MET =="<<MET<<"\n";
	std::cout<<"mTW =="<<mTW<<"\n";
	std::cout<<"j1Flav =="<<j1Flav<<"\n";
	std::cout<<"j2Flav =="<<j2Flav<<"\n";
	//continue;
	//m_weight = (m_weight / fabs(MCWeight) ); //Set MCWeight==1 or -1 for large weight event.
	//No need for MC16a and MC16c since all the fixs have been done in the Reader
	}
     }

    //Add selections here.
 //mu>35 high mu    
	 if(ActualMuScaled>35) continue;
    
	//if(nTaus!=0) continue; //Tau Veto!!!

	if(!(nSJ>=2)) continue; //NSignalJet>=2
    if(pTV<150 && mTW<=20) continue;//True! //mTW>20 for medium pTV region
    if(MET<=30 && nElectrons==1) continue;  //MET>30 for electron channel

	//Cut Based Selection
   if(m_CBA){	
	if(pTV<150){
	 if(dRBB>=3.0) continue;
	 }
	if(pTV>=150&pTV<200){
	 if(dRBB>=1.8) continue;
	}
	if(pTV>=200){
	 if(dRBB>=1.2) continue;
	}
	if(mTW>=120)continue; 
   }
	//Cut Based Selection
   if(m_MJ && m_TightIso){
	if(nElectrons==1 && topoetcone20>=12e3) continue; //11 6 12
	if(nElectrons==0 && ptcone20>=2.9e3) continue;//2.25 2.1 2.9
   }
   
   if(!m_MC16a){
     if( (m_DSID.Contains("singletop_t")) ) m_weight *=1.2; //Yanhui
     if( (m_DSID.Contains("singletop_s")) ) m_weight *=1.624;//Yanhui
   }
   if( m_MJ && (!m_DSID.Contains("data")) ) m_weight *=-1;
   if( m_MJ && m_UseSFs) {
	if(m_DSID.Contains("ttbar") || m_DSID.Contains("singletop_t") || m_DSID.Contains("singletop_s") || m_DSID.Contains("singletop_Wt")){
	 if(pTV>=150){ 
	  if(nJets==2) m_weight *=1.0226e+00;
	  else if (nJets==3) m_weight *=9.9672e-01;
	 }
	 else {
	  if(nJets==2) m_weight *=1.0439e+00;
	  else if (nJets==3) m_weight *=1.0728e+00;
	 }
    }
	else if ( m_DSID.Contains("Wenu") || m_DSID.Contains("Wmunu") || m_DSID.Contains("Wtaunu") ){
     if(pTV>=150){
      if(nJets==2) m_weight *=1.2553e+00;
      else if (nJets==3) m_weight *=1.1328e+00;
     }
     else {
      if(nJets==2) m_weight *=1.4852e+00;
      else if (nJets==3) m_weight *=1.0863e+00;
     }
    }
   } 	
   //Set the region description
	if( mBB < 75 && Mtop > 225 ) {
      m_histFill->SetDescription("WhfCR");
    }
    else {
      m_histFill->SetDescription("WhfSR");
    }
    m_histFill->SetNjets(nJets);
    m_histFill->SetNFjets(nFJ);
    m_histFill->SetpTV(pTV);  
	m_histFill->SetNtags(nTags);
    bool isEl = false;
	if (nElectrons==1) isEl = true;
	bool isCBA = false;
	if(m_CBA) isCBA = true;
    //Fill the histograms
    if(m_Debug) std::cout << "Filling histograms" << std::endl;
    m_histFill->BookFillHist("mBBCutBased", isEl,isCBA,100,  0,500,  mBB,    m_weight);
  	
    m_histFill->BookFillHist("lmetdPhi",isEl,isCBA,400,0,4, lmetdPhi,    m_weight); 
    m_histFill->BookFillHist("jmetdPhi",isEl,isCBA,400,0,4, jmetdPhi,    m_weight);
    m_histFill->BookFillHist("jldPhi",isEl,isCBA,400,0,4, jldPhi,    m_weight);
    //m_histFill->BookFillHist("jjmindPhi",isEl,isCBA,400,0,4, jjmindPhi,    m_weight); 
    m_histFill->BookFillHist("topoetcone20",isEl,isCBA,35e3,  -5e3,30e3, topoetcone20,    m_weight); 
    m_histFill->BookFillHist("ptcone20",isEl,isCBA,30e3,  0,30e3, ptcone20,    m_weight); 
    m_histFill->BookFillHist("d0sigBL",isEl,isCBA,100,  -5,5, d0sigBL,    m_weight); 
    m_histFill->BookFillHist("z0sinTheta",isEl,isCBA,100,  -5,5, z0sinTheta,    m_weight); 
    
	m_histFill->BookFillHist("jjmetdPhi",isEl,isCBA,400,0,4, jjmetdPhi,    m_weight); 
	m_histFill->BookFillHist("jjjmetdPhi",isEl,isCBA,400,0,4, jjjmetdPhi,    m_weight);
	m_histFill->BookFillHist("jjldPhi",isEl,isCBA,400,0,4, jjldPhi,    m_weight);
	m_histFill->BookFillHist("jjjldPhi",isEl,isCBA,400,0,4, jjjldPhi,    m_weight);

	m_histFill->BookFillHist("mva",     isEl,isCBA,1000,  -1,1,  mva,    m_weight); 
	m_histFill->BookFillHist("mvadiboson",     isEl,isCBA,1000,  -1,1,  mvadiboson,    m_weight);
	m_histFill->BookFillHist("mva_21",     isEl,isCBA,1000,  -1,1,  mva_21,    m_weight);
	m_histFill->BookFillHist("mvadiboson_21",     isEl,isCBA,1000,  -1,1,  mvadiboson_21,    m_weight);
	m_histFill->BookFillHist("MET",     isEl,isCBA,100,  0,1000,  MET,    m_weight);    
    m_histFill->BookFillHist("mBB",     isEl,isCBA,100,  0,500,  mBB,    m_weight);
    m_histFill->BookFillHist("pTV",     isEl,isCBA,200,  0,2000,  pTV,    m_weight);
    m_histFill->BookFillHist("mTW",     isEl,isCBA,100,  0,500,  mTW,    m_weight);
    m_histFill->BookFillHist("mBBJ",     isEl,isCBA,100,  0,500, mBBJ,    m_weight);
    m_histFill->BookFillHist("Mtop",     isEl,isCBA,500,  0,500, Mtop,    m_weight);
    m_histFill->BookFillHist("dYWH",     isEl,isCBA,100,  0,6,   dYWH,    m_weight);
    m_histFill->BookFillHist("dRBB",     isEl,isCBA,100,  0,6,   dRBB,    m_weight);
    
	m_histFill->BookFillHist("mindPhiLJ",isEl,isCBA,400,0,4, mindPhiLJ,    m_weight);
    m_histFill->BookFillHist("dPhiVBB",  isEl,isCBA,400,0,4, dPhiVBB,    m_weight); 
    m_histFill->BookFillHist("Ptl",     isEl,isCBA,1000,  0,1000,  Ptl,    m_weight);
    m_histFill->BookFillHist("Etal",     isEl,isCBA,300,  0,3,  fabs(Etal),    m_weight);
    m_histFill->BookFillHist("Phil",     isEl,isCBA,800,  -4,4,  Phil,    m_weight);
    m_histFill->BookFillHist("PtB1",     isEl,isCBA,1000,  0,1000,  PtB1,    m_weight);
    m_histFill->BookFillHist("EtaB1",     isEl,isCBA,100,  -3,3,  EtaB1,    m_weight);
    m_histFill->BookFillHist("PhiB1",     isEl,isCBA,800,  -4,4,  PhiB1,    m_weight);
    m_histFill->BookFillHist("PtB2",     isEl,isCBA,1000,  0,1000,  PtB2,    m_weight);
    m_histFill->BookFillHist("EtaB2",     isEl,isCBA,100,  -3,3,  EtaB2,    m_weight);
    m_histFill->BookFillHist("PhiB2",     isEl,isCBA,800,  -4,4,  PhiB2,    m_weight);
    m_histFill->BookFillHist("PtJ3",     isEl,isCBA,1000,  0,1000,  PtJ3,    m_weight);
    m_histFill->BookFillHist("EtaJ3",     isEl,isCBA,100,  -5,5,  EtaJ3,    m_weight);
    m_histFill->BookFillHist("PhiJ3",     isEl,isCBA,800,  -4,4,  PhiJ3,    m_weight);
    m_histFill->BookFillHist("nJets",   isEl,isCBA,11,   -0.5,10.5,   nJets, m_weight);
	m_histFill->BookFillHist("nTaus",   isEl,isCBA,11,   -0.5,10.5,   nTaus, m_weight);
    m_histFill->BookFillHist("nSJ",   isEl,isCBA,11,   -0.5,10.5,   nSJ, m_weight);
	m_histFill->BookFillHist("nFJ",   isEl,isCBA,11,   -0.5,10.5,   nFJ, m_weight);
    
	//m_histFill->BookFillHist("HtRatio",   isEl,isCBA,100,   0.0,1.0,   HtRatio, m_weight);
    if( m_DSID.Contains("data")){
    //m_histFill->BookFillHist("mu",   isEl,isCBA,100,   0,100.,   mu/1.03, m_weight);
    m_histFill->BookFillHist("mu",   isEl,isCBA,100,   0,100.,   ActualMuScaled, m_weight);
	//m_histFill->BookFillHist("munJets", isEl,isCBA,100,   0,100.,mu/1.09, 11,-0.5,10.5,nJets, m_weight);
    //m_histFill->BookFillHist("munSJ", isEl,isCBA,100,   0,100.,mu/1.09, 11,-0.5,10.5,nSJ, m_weight);
    //m_histFill->BookFillHist("munFJ", isEl,isCBA,100,   0,100.,mu/1.09, 11,-0.5,10.5,nFJ, m_weight);

    }
    else{
    m_histFill->BookFillHist("mu",   isEl,isCBA,100,   0,100.,   ActualMuScaled, m_weight);
    //m_histFill->BookFillHist("munJets", isEl,isCBA,100,   0,100.,mu, 11,-0.5,10.5,nJets, m_weight);
    //m_histFill->BookFillHist("munSJ", isEl,isCBA,100,   0,100.,mu, 11,-0.5,10.5,nSJ, m_weight);
    //m_histFill->BookFillHist("munFJ", isEl,isCBA,100,   0,100.,mu, 11,-0.5,10.5,nFJ, m_weight);
    }
	//m_histFill->BookFillHist("pTVMBB", isEl,isCBA,100, 0, 500, pTV, 100, 0, 500, mBB, m_weight);
    
	if(nElectrons==1){
    
	m_histFill->BookFillHist("jjmetdPhi_El",isEl,isCBA,400,0,4, jjmetdPhi,    m_weight);
    m_histFill->BookFillHist("jjjmetdPhi_El",isEl,isCBA,400,0,4, jjjmetdPhi,    m_weight);
    m_histFill->BookFillHist("jjldPhi_El",isEl,isCBA,400,0,4, jjldPhi,    m_weight);
    m_histFill->BookFillHist("jjjldPhi_El",isEl,isCBA,400,0,4, jjjldPhi,    m_weight);
    m_histFill->BookFillHist("lmetdPhi_El",isEl,isCBA,400,0,4, lmetdPhi,    m_weight);    
    m_histFill->BookFillHist("jmetdPhi_El",isEl,isCBA,400,0,4, jmetdPhi,    m_weight);
    m_histFill->BookFillHist("jldPhi_El",isEl,isCBA,400,0,4, jldPhi,    m_weight);
    //m_histFill->BookFillHist("jjmindPhi_El",isEl,isCBA,400,0,4, jjmindPhi,    m_weight);
    m_histFill->BookFillHist("topoetcone20_El",isEl,isCBA,35e3,  -5e3,30e3, topoetcone20,    m_weight); 
    m_histFill->BookFillHist("ptcone20_El",isEl,isCBA,30e3,  0,30e3, ptcone20,    m_weight);
    m_histFill->BookFillHist("d0sigBL_El",isEl,isCBA,100,  -5,5, d0sigBL,    m_weight);   
    m_histFill->BookFillHist("z0sinTheta_El",isEl,isCBA,100,  -5,5, z0sinTheta,    m_weight);

   
    m_histFill->BookFillHist("mva_El",     isEl,isCBA,1000,  -1,1,  mva,    m_weight);
	m_histFill->BookFillHist("mvadiboson_El",     isEl,isCBA,1000,  -1,1,  mvadiboson,    m_weight);
    m_histFill->BookFillHist("mva_21_El",     isEl,isCBA,1000,  -1,1,  mva_21,    m_weight);
    m_histFill->BookFillHist("mvadiboson_21_El",     isEl,isCBA,1000,  -1,1,  mvadiboson_21,    m_weight);
	m_histFill->BookFillHist("MET_El",     isEl,isCBA,100,  0,1000,  MET,    m_weight);
    m_histFill->BookFillHist("mBB_El",     isEl,isCBA,100,  0,500,  mBB,    m_weight);
    m_histFill->BookFillHist("pTV_El",     isEl,isCBA,200,  0,2000,  pTV,    m_weight);
    m_histFill->BookFillHist("mTW_El",     isEl,isCBA,100,  0,500,  mTW,    m_weight);
    m_histFill->BookFillHist("mBBJ_El",     isEl,isCBA,100,  0,500, mBBJ,    m_weight);
    m_histFill->BookFillHist("Mtop_El",     isEl,isCBA,500,  0,500, Mtop,    m_weight);
    m_histFill->BookFillHist("dYWH_El",     isEl,isCBA,100,  0,6,   dYWH,    m_weight);
    m_histFill->BookFillHist("dRBB_El",     isEl,isCBA,100,  0,6,   dRBB,    m_weight);
    m_histFill->BookFillHist("mindPhiLJ_El",isEl,isCBA,400,0,4, mindPhiLJ,    m_weight);
	m_histFill->BookFillHist("dPhiVBB_El",  isEl,isCBA,400,0,4, dPhiVBB,    m_weight);
    m_histFill->BookFillHist("Ptl_El",     isEl,isCBA,1000,  0,1000,  Ptl,    m_weight);
    m_histFill->BookFillHist("Etal_El",     isEl,isCBA,300,  0,3,  fabs(Etal),    m_weight);
    m_histFill->BookFillHist("Phil_El",     isEl,isCBA,800,  -4,4,  Phil,    m_weight);
    m_histFill->BookFillHist("PtB1_El",     isEl,isCBA,1000,  0,1000,  PtB1,    m_weight);
    m_histFill->BookFillHist("EtaB1_El",     isEl,isCBA,100,  -3,3,  EtaB1,    m_weight);
    m_histFill->BookFillHist("PhiB1_El",     isEl,isCBA,800,  -4,4,  PhiB1,    m_weight);
    m_histFill->BookFillHist("PtB2_El",     isEl,isCBA,1000,  0,1000,  PtB2,    m_weight);
    m_histFill->BookFillHist("EtaB2_El",     isEl,isCBA,100,  -3,3,  EtaB2,    m_weight);
    m_histFill->BookFillHist("PhiB2_El",     isEl,isCBA,800,  -4,4,  PhiB2,    m_weight);
    m_histFill->BookFillHist("PtJ3_El",     isEl,isCBA,1000,  0,1000,  PtJ3,    m_weight);
    m_histFill->BookFillHist("EtaJ3_El",     isEl,isCBA,100,  -5,5,  EtaJ3,    m_weight);
    m_histFill->BookFillHist("PhiJ3_El",     isEl,isCBA,800,  -4,4,  PhiJ3,    m_weight);
    m_histFill->BookFillHist("nJets_El",   isEl,isCBA,11,   -0.5,10.5,   nJets, m_weight);
	m_histFill->BookFillHist("nTaus_El",   isEl,isCBA,11,   -0.5,10.5,   nTaus, m_weight);
    m_histFill->BookFillHist("nSJ_El",   isEl,isCBA,11,   -0.5,10.5,   nSJ, m_weight);
	m_histFill->BookFillHist("nFJ_El",   isEl,isCBA,11,   -0.5,10.5,   nFJ, m_weight);
	
	//m_histFill->BookFillHist("HtRatio_El",   isEl,isCBA,100,   0.0,1.0,   HtRatio, m_weight);
	if( m_DSID.Contains("data")){
    //m_histFill->BookFillHist("mu_El",   isEl,isCBA,100,   0,100.,   mu/1.03, m_weight);
	m_histFill->BookFillHist("mu_El",   isEl,isCBA,100,   0,100.,   ActualMuScaled, m_weight);
	//m_histFill->BookFillHist("munJets_El", isEl,isCBA,100,   0,100.,mu/1.09, 11,-0.5,10.5,nJets, m_weight);
	//m_histFill->BookFillHist("munSJ_El", isEl,isCBA,100,   0,100.,mu/1.09, 11,-0.5,10.5,nSJ, m_weight);
	//m_histFill->BookFillHist("munFJ_El", isEl,isCBA,100,   0,100.,mu/1.09, 11,-0.5,10.5,nFJ, m_weight);

	}
    else{
    m_histFill->BookFillHist("mu_El",   isEl,isCBA,100,   0,100.,   ActualMuScaled, m_weight);
	//m_histFill->BookFillHist("munJets_El", isEl,isCBA,100,   0,100.,mu, 11,-0.5,10.5,nJets, m_weight);
	//m_histFill->BookFillHist("munSJ_El", isEl,isCBA,100,   0,100.,mu, 11,-0.5,10.5,nSJ, m_weight);
	//m_histFill->BookFillHist("munFJ_El", isEl,isCBA,100,   0,100.,mu, 11,-0.5,10.5,nFJ, m_weight);
	}
	//m_histFill->BookFillHist("pTVMBB_El", isEl,isCBA,100, 0, 500, pTV, 100, 0, 500, mBB, m_weight);	
	}
    else if (nElectrons==0){
    
	m_histFill->BookFillHist("jjmetdPhi_Mu",isEl,isCBA,400,0,4, jjmetdPhi,    m_weight);
    m_histFill->BookFillHist("jjjmetdPhi_Mu",isEl,isCBA,400,0,4, jjjmetdPhi,    m_weight);
    m_histFill->BookFillHist("jjldPhi_Mu",isEl,isCBA,400,0,4, jjldPhi,    m_weight);
    m_histFill->BookFillHist("jjjldPhi_Mu",isEl,isCBA,400,0,4, jjjldPhi,    m_weight);
    m_histFill->BookFillHist("lmetdPhi_Mu",isEl,isCBA,400,0,4, lmetdPhi,    m_weight);
    m_histFill->BookFillHist("jmetdPhi_Mu",isEl,isCBA,400,0,4, jmetdPhi,    m_weight);
    m_histFill->BookFillHist("jldPhi_Mu",isEl,isCBA,400,0,4, jldPhi,    m_weight);
    //m_histFill->BookFillHist("jjmindPhi_Mu",isEl,isCBA,400,0,4, jjmindPhi,    m_weight);
    m_histFill->BookFillHist("topoetcone20_Mu",isEl,isCBA,35e3,  -5e3,30e3, topoetcone20,    m_weight);
    m_histFill->BookFillHist("ptcone20_Mu",isEl,isCBA,30e3,  0,30e3, ptcone20,    m_weight);
    m_histFill->BookFillHist("d0sigBL_Mu",isEl,isCBA,100,  -5,5, d0sigBL,    m_weight);
    m_histFill->BookFillHist("z0sinTheta_Mu",isEl,isCBA,100,  -5,5, z0sinTheta,    m_weight); 


    m_histFill->BookFillHist("mva_Mu",     isEl,isCBA,1000,  -1,1,  mva,    m_weight);
	m_histFill->BookFillHist("mvadiboson_Mu",     isEl,isCBA,1000,  -1,1,  mvadiboson,    m_weight);
	m_histFill->BookFillHist("mva_21_Mu",     isEl,isCBA,1000,  -1,1,  mva_21,    m_weight);
    m_histFill->BookFillHist("mvadiboson_21_Mu",     isEl,isCBA,1000,  -1,1,  mvadiboson_21,    m_weight);
    m_histFill->BookFillHist("MET_Mu",     isEl,isCBA,100,  0,1000,  MET,    m_weight);
    m_histFill->BookFillHist("mBB_Mu",     isEl,isCBA,100,  0,500,  mBB,    m_weight);
    m_histFill->BookFillHist("pTV_Mu",     isEl,isCBA,200,  0,2000,  pTV,    m_weight);
    m_histFill->BookFillHist("mTW_Mu",     isEl,isCBA,100,  0,500,  mTW,    m_weight);
    m_histFill->BookFillHist("mBBJ_Mu",     isEl,isCBA,100,  0,500, mBBJ,    m_weight);
    m_histFill->BookFillHist("Mtop_Mu",     isEl,isCBA,500,  0,500, Mtop,    m_weight);
    m_histFill->BookFillHist("dYWH_Mu",     isEl,isCBA,100,  0,6,   dYWH,    m_weight);
    m_histFill->BookFillHist("dRBB_Mu",     isEl,isCBA,100,  0,6,   dRBB,    m_weight);
    m_histFill->BookFillHist("mindPhiLJ_Mu",isEl,isCBA,400,0,4, mindPhiLJ,    m_weight);
	m_histFill->BookFillHist("dPhiVBB_Mu",  isEl,isCBA,400,0,4, dPhiVBB,    m_weight);
    m_histFill->BookFillHist("Ptl_Mu",     isEl,isCBA,1000,  0,1000,  Ptl,    m_weight);
    m_histFill->BookFillHist("Etal_Mu",     isEl,isCBA,300,  0,3,  fabs(Etal),    m_weight);
    m_histFill->BookFillHist("Phil_Mu",     isEl,isCBA,800,  -4,4,  Phil,    m_weight);
    m_histFill->BookFillHist("PtB1_Mu",     isEl,isCBA,1000,  0,1000,  PtB1,    m_weight);
    m_histFill->BookFillHist("EtaB1_Mu",     isEl,isCBA,100,  -3,3,  EtaB1,    m_weight);
    m_histFill->BookFillHist("PhiB1_Mu",     isEl,isCBA,800,  -4,4,  PhiB1,    m_weight);
    m_histFill->BookFillHist("PtB2_Mu",     isEl,isCBA,1000,  0,1000,  PtB2,    m_weight);
    m_histFill->BookFillHist("EtaB2_Mu",     isEl,isCBA,100,  -3,3,  EtaB2,    m_weight);
    m_histFill->BookFillHist("PhiB2_Mu",     isEl,isCBA,800,  -4,4,  PhiB2,    m_weight);
    m_histFill->BookFillHist("PtJ3_Mu",     isEl,isCBA,1000,  0,1000,  PtJ3,    m_weight);
    m_histFill->BookFillHist("EtaJ3_Mu",     isEl,isCBA,100,  -5,5,  EtaJ3,    m_weight);
    m_histFill->BookFillHist("PhiJ3_Mu",     isEl,isCBA,800,  -4,4,  PhiJ3,    m_weight);
    m_histFill->BookFillHist("nJets_Mu",   isEl,isCBA,11,   -0.5,10.5,   nJets, m_weight);
	m_histFill->BookFillHist("nTaus_Mu",   isEl,isCBA,11,   -0.5,10.5,   nTaus, m_weight);
    m_histFill->BookFillHist("nSJ_Mu",   isEl,isCBA,11,   -0.5,10.5,   nSJ, m_weight);
	m_histFill->BookFillHist("nFJ_Mu",   isEl,isCBA,11,   -0.5,10.5,   nFJ, m_weight);
	
	//m_histFill->BookFillHist("HtRatio_Mu",   isEl,isCBA,100,   0.0,1.0,   HtRatio, m_weight);
	
	
    if( m_DSID.Contains("data")){
    //m_histFill->BookFillHist("mu_Mu",   isEl,isCBA,100,   0,100.,   mu/1.03, m_weight);
    m_histFill->BookFillHist("mu_Mu",   isEl,isCBA,100,   0,100.,   ActualMuScaled, m_weight);
	//m_histFill->BookFillHist("munJets_Mu", isEl,isCBA,100,   0,100.,mu/1.09, 11,-0.5,10.5,nJets, m_weight);
    //m_histFill->BookFillHist("munSJ_Mu", isEl,isCBA,100,   0,100.,mu/1.09, 11,-0.5,10.5,nSJ, m_weight);
    //m_histFill->BookFillHist("munFJ_Mu", isEl,isCBA,100,   0,100.,mu/1.09, 11,-0.5,10.5,nFJ, m_weight);

    }
    else{
    m_histFill->BookFillHist("mu_Mu",   isEl,isCBA,100,   0,100.,   ActualMuScaled, m_weight);
    //m_histFill->BookFillHist("munJets_Mu", isEl,isCBA,100,   0,100.,mu, 11,-0.5,10.5,nJets, m_weight);
    //m_histFill->BookFillHist("munSJ_Mu", isEl,isCBA,100,   0,100.,mu, 11,-0.5,10.5,nSJ, m_weight);
    //m_histFill->BookFillHist("munFJ_Mu", isEl,isCBA,100,   0,100.,mu, 11,-0.5,10.5,nFJ, m_weight);
    }
    //m_histFill->BookFillHist("pTVMBB_Mu", isEl,isCBA,100, 0, 500, pTV, 100, 0, 500, mBB, m_weight);
    }
    else {
    std::cout<<"Should not, no electron or muon found!"<<"\n";
    }
      
    
  }
}


