//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Oct 25 16:20:48 2017 by ROOT version 5.34/36
// from TTree ReadTruthTree/
// found on file: ntuple.root
//////////////////////////////////////////////////////////

#ifndef ReadTruthTree_h
#define ReadTruthTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <string.h>
#include <iostream>
#include <fstream>
using namespace std;
// Fixed size dimensions of array or collections stored in the TTree if any.

class ReadTruthTree {
//using namespace std;
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   float EventWeight;
   float MCWeight;
   unsigned long long RunNumber;
   unsigned long long EventNumber;
   float jjM;
   float WPt;
   float Mtop;
   float WM;
   int nJets;
   int nTags;
   int nSJ;
   int nFJ;
   int nTaus;
   int j1Flav;
   int j2Flav;
   int nElectrons;
   float lPt;
   float lEta;
   float lPhi;
   float j1Pt;
   float j1Eta;
   float j1Phi;
   float j2Pt;
   float j2Eta;
   float j2Phi;
   float j3Pt;
   float j3Eta;
   float j3Phi;
   float met;
   float dYWH;
   float jjdR;
   float jjjM;
   float ljmindPhi;
   float WjjdPhi;
   float mu;
   float AverageMuScaled;
			float AverageMu;
			float ActualMuScaled;
			float ActualMu;
   float BDT;
   float BDTWZ;
   float BDT_21;
   float BDTWZ_21;

   float  lmetdPhi;
   float  jmetdPhi;
   float  jldPhi;
   
   //double jjmindPhi;

   float  topoetcone20;
   float  ptcone20;
   float  d0sigBL;
   float  z0sinTheta;
  
   float jjmetdPhi;
   float jjjmetdPhi;
   float jjldPhi;
   float jjjldPhi;
   std::vector<float>   *SJet_Pt;//Yanhui
   std::vector<float>   *SJet_Eta;
   std::vector<float>   *SJet_Jvt;
   std::vector<float>   *SJet_JvtSF;
   std::vector<bool>    *SJet_isTagged;
   std::vector<float>   *FJet_Pt;
   std::vector<float>   *FJet_Eta;
   std::vector<float>   *FJet_Jvt;
   std::vector<float>   *FJet_JvtSF;
   std::vector<bool>    *FJet_JvtLoose;
   std::vector<bool>    *FJet_JvtTight;
   std::vector<int>     *SJet_PartonTruthLabelID;
   std::vector<int>     *FJet_PartonTruthLabelID;
   TBranch        *b_FJet_Pt;   //!
   TBranch        *b_FJet_Eta;   //!
   TBranch        *b_FJet_Jvt;   //!
   TBranch        *b_FJet_JvtSF;   //!
   TBranch        *b_FJet_JvtLoose;   //!
   TBranch        *b_FJet_JvtTight;   //!
   TBranch        *b_SJet_Pt;   //!
   TBranch        *b_SJet_Eta;   //!
   TBranch        *b_SJet_Jvt;   //!
   TBranch        *b_SJet_JvtSF;   //!
   TBranch        *b_SJet_isTagged;   //!
   TBranch        *b_SJet_PartonTruthLabelID;   //!
   TBranch        *b_FJet_PartonTruthLabelID;   //!
   TBranch        *b_jjmetdPhi;   //!
   TBranch        *b_jjjmetdPhi;   //!
   TBranch        *b_jjldPhi;   //!
   TBranch        *b_jjjldPhi;   //!

   TBranch        *b_lmetdPhi;   //!
   TBranch        *b_jmetdPhi;   //!
   TBranch        *b_jldPhi;   //!
   //TBranch        *b_jjmindPhi;   //!
   
   TBranch        *b_topoetcone20;   //!
   
   TBranch        *b_ptcone20;   //!
   TBranch        *b_d0sigBL;   //!
   TBranch        *b_z0sinTheta;   //!

   TBranch        *b_BDT;   //!
   TBranch        *b_BDTWZ;   //!
   TBranch        *b_BDT_21;   //!
   TBranch        *b_BDTWZ_21;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_AverageMuScaled;   //!
			TBranch        *b_AverageMu;   //!
			TBranch        *b_ActualMuScaled;   //!
			TBranch        *b_ActualMu;   //!
   TBranch        *b_nTags;   //!
   TBranch        *b_nSJ;   //!
   TBranch        *b_nFJ;   //!
   TBranch        *b_nTaus;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_MCWeight;   //!
   TBranch        *b_nElectrons;   //!
   TBranch        *b_WjjdPhi;   //!
   TBranch        *b_ljmindPhi;   //!
   TBranch        *b_jjjM;   //!
   TBranch        *b_jjdR;   //!
   TBranch        *b_dYWH;   //!
   TBranch        *b_met;   //!
   TBranch        *b_j3Pt;   //!
   TBranch        *b_j3Eta;   //!
   TBranch        *b_j3Phi;   //!
   TBranch        *b_j2Pt;   //!
   TBranch        *b_j2Eta;   //!
   TBranch        *b_j2Phi;   //!
   TBranch        *b_j1Pt;   //!
   TBranch        *b_j1Eta;   //!
   TBranch        *b_j1Phi;   //!
   TBranch        *b_lPt;   //!
   TBranch        *b_lEta;   //!
   TBranch        *b_lPhi;   //!
   TBranch        *b_j1Flav;   //!
   TBranch        *b_j2Flav;   //!
   TBranch        *b_WM;   //!
   TBranch        *b_EventWeight;   //!
   TBranch        *b_jjM;   //!
   TBranch        *b_WPt;   //!
   TBranch        *b_nJets;   //!
   TBranch        *b_Mtop;   //!


   ReadTruthTree(TTree *tree=0);
   virtual ~ReadTruthTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef ReadTruthTree_cxx
ReadTruthTree::ReadTruthTree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
/*
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("ntuple.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("ntuple.root");
      }
      f->GetObject("ReadTruthTree",tree);

   }
*/
//std::cout<<"WTF?"<<"\n";
Init(tree);

}

ReadTruthTree::~ReadTruthTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ReadTruthTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ReadTruthTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ReadTruthTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   nTags=-99;
   nSJ=-99;
   nFJ=-99;
   nTaus=-99;
   mu=-99;
   AverageMuScaled=-99;
			AverageMu=-99;
			ActualMuScaled=-99;
			ActualMu=-99;
   MCWeight=-999;
   RunNumber=-99;
   EventNumber=-99;
   nElectrons=-99;
   EventWeight=-99;
   jjM=-99;
   WPt=-99;
   Mtop=-99;
   WM=-99;
   nJets=-99;
   j1Flav=-99;
   j2Flav=-99;
   lPt=-99;
   lEta=-99;
   lPhi=-99;
   j1Pt=-99;
   j1Eta=-99;
   j1Phi=-99;
   j2Pt=-99;
   j2Eta=-99;
   j2Phi=-99;
   j3Pt=-99;
   j3Eta=-99;
   j3Phi=-99;
   met=-99;
   dYWH=-99;
   jjdR=-99;
   jjjM=-99;
   ljmindPhi=-99;
   WjjdPhi=-99;   
   BDT=-99;
   BDTWZ=-99;
   BDT_21=-99;
   BDTWZ_21=-99;
 
   lmetdPhi=-99;
   jmetdPhi=-99;
   jldPhi=-99;
   //jjmindPhi=-99;
   
   topoetcone20=-99;
   
   ptcone20=-99;
   d0sigBL=-99;
   z0sinTheta=-99;
   
   jjmetdPhi=-99;
   jjjmetdPhi=-99;
   jjldPhi=-99;
   jjjldPhi=-99;
   SJet_Pt=0;//Yanhui
   SJet_Eta=0;
   SJet_Jvt=0;
   SJet_JvtSF=0;
   SJet_isTagged=0;
   FJet_Pt=0;
   FJet_Eta=0;
   FJet_Jvt=0;
   FJet_JvtSF=0;
   FJet_JvtLoose=0;
   FJet_JvtTight=0;
   SJet_PartonTruthLabelID=0;
   FJet_PartonTruthLabelID=0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);
   fChain->SetBranchAddress("SJet_Pt", &SJet_Pt, &b_SJet_Pt);
   fChain->SetBranchAddress("SJet_Eta", &SJet_Eta, &b_SJet_Eta);
   fChain->SetBranchAddress("SJet_Jvt", &SJet_Jvt, &b_SJet_Jvt);
   fChain->SetBranchAddress("SJet_JvtSF", &SJet_JvtSF, &b_SJet_JvtSF);
   fChain->SetBranchAddress("SJet_isTagged", &SJet_isTagged, &b_SJet_isTagged);
   fChain->SetBranchAddress("FJet_Pt", &FJet_Pt, &b_FJet_Pt);
   fChain->SetBranchAddress("FJet_Eta", &FJet_Eta, &b_FJet_Eta);
   fChain->SetBranchAddress("FJet_Jvt", &FJet_Jvt, &b_FJet_Jvt);
   fChain->SetBranchAddress("FJet_JvtSF", &FJet_JvtSF, &b_FJet_JvtSF);
   fChain->SetBranchAddress("FJet_JvtLoose", &FJet_JvtLoose, &b_FJet_JvtLoose);
   fChain->SetBranchAddress("FJet_JvtTight", &FJet_JvtTight, &b_FJet_JvtTight);
   fChain->SetBranchAddress("SJet_PartonTruthLabelID", &SJet_PartonTruthLabelID, &b_SJet_PartonTruthLabelID);
   fChain->SetBranchAddress("FJet_PartonTruthLabelID", &FJet_PartonTruthLabelID, &b_FJet_PartonTruthLabelID);
   fChain->SetBranchAddress("jjmetdPhi",  &jjmetdPhi,  &b_jjmetdPhi);
   fChain->SetBranchAddress("jjjmetdPhi",  &jjjmetdPhi,  &b_jjjmetdPhi);
   fChain->SetBranchAddress("jjldPhi",  &jjldPhi,  &b_jjldPhi);
   fChain->SetBranchAddress("jjjldPhi",  &jjjldPhi,  &b_jjjldPhi);

   fChain->SetBranchAddress("lmetdPhi",  &lmetdPhi,  &b_lmetdPhi);
   fChain->SetBranchAddress("jmetdPhi",  &jmetdPhi,  &b_jmetdPhi);
   fChain->SetBranchAddress("jldPhi",    &jldPhi,    &b_jldPhi);
   //fChain->SetBranchAddress("jjmindPhi", &jjmindPhi, &b_jjmindPhi);
   fChain->SetBranchAddress("topoetcone20", &topoetcone20, &b_topoetcone20);
   fChain->SetBranchAddress("ptcone20", &ptcone20, &b_ptcone20);
   fChain->SetBranchAddress("d0sigBL", &d0sigBL, &b_d0sigBL);
   fChain->SetBranchAddress("z0sinTheta", &z0sinTheta, &b_z0sinTheta);

   fChain->SetBranchAddress("BDT", &BDT, &b_BDT);
   fChain->SetBranchAddress("BDTWZ", &BDTWZ, &b_BDTWZ);
   fChain->SetBranchAddress("BDT_21", &BDT_21, &b_BDT_21);
   fChain->SetBranchAddress("BDTWZ_21", &BDTWZ_21, &b_BDTWZ_21);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("MCWeight", &MCWeight, &b_MCWeight);
   fChain->SetBranchAddress("nElectrons", &nElectrons, &b_nElectrons);
   fChain->SetBranchAddress("lPt", &lPt, &b_lPt);
   fChain->SetBranchAddress("lEta", &lEta, &b_lEta);
   fChain->SetBranchAddress("lPhi", &lPhi, &b_lPhi);
   fChain->SetBranchAddress("j1Pt", &j1Pt, &b_j1Pt);
   fChain->SetBranchAddress("j1Eta", &j1Eta, &b_j1Eta);
   fChain->SetBranchAddress("j1Phi", &j1Phi, &b_j1Phi);
   fChain->SetBranchAddress("j2Pt", &j2Pt, &b_j2Pt);
   fChain->SetBranchAddress("j2Eta", &j2Eta, &b_j2Eta);
   fChain->SetBranchAddress("j2Phi", &j2Phi, &b_j2Phi);
   fChain->SetBranchAddress("j3Pt", &j3Pt, &b_j3Pt);
   fChain->SetBranchAddress("j3Eta", &j3Eta, &b_j3Eta);
   fChain->SetBranchAddress("j3Phi", &j3Phi, &b_j3Phi);
   fChain->SetBranchAddress("j1Flav", &j1Flav, &b_j1Flav);
   fChain->SetBranchAddress("j2Flav", &j2Flav, &b_j2Flav);
   fChain->SetBranchAddress("EventWeight", &EventWeight, &b_EventWeight);
   fChain->SetBranchAddress("jjM", &jjM, &b_jjM);
   fChain->SetBranchAddress("WPt", &WPt, &b_WPt);
   fChain->SetBranchAddress("Mtop", &Mtop, &b_Mtop);
   fChain->SetBranchAddress("dYWH", &dYWH, &b_dYWH);  
   fChain->SetBranchAddress("jjjM", &jjjM, &b_jjjM);
   fChain->SetBranchAddress("jjdR", &jjdR, &b_jjdR);
   fChain->SetBranchAddress("ljmindPhi", &ljmindPhi, &b_ljmindPhi);
   fChain->SetBranchAddress("WjjdPhi", &WjjdPhi, &b_WjjdPhi);
   fChain->SetBranchAddress("met", &met, &b_met);
   fChain->SetBranchAddress("nJets", &nJets, &b_nJets);
   //std::cout<<"LLLLL1"<<"\n";
   fChain->SetBranchAddress("nTags", &nTags, &b_nTags);
   fChain->SetBranchAddress("nTaus", &nTaus, &b_nTaus);
   fChain->SetBranchAddress("nSJ", &nSJ, &b_nSJ);
   fChain->SetBranchAddress("nFJ", &nFJ, &b_nFJ);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("AverageMuScaled", &AverageMuScaled, &b_AverageMuScaled);
			fChain->SetBranchAddress("AverageMu",       &AverageMu,       &b_AverageMu);
			fChain->SetBranchAddress("ActualMuScaled",  &ActualMuScaled,  &b_ActualMuScaled);
			fChain->SetBranchAddress("ActualMu",        &ActualMu,        &b_ActualMu);
   fChain->SetBranchAddress("WM", &WM, &b_WM);
   Notify();
}

Bool_t ReadTruthTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ReadTruthTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ReadTruthTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ReadTruthTree_cxx
