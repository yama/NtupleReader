#ifndef MVATree_H
#define MVATree_H

#include <string>
#include <vector>
#include <map>

#include "TString.h"
#include "TH1.h"
#include "TH2.h"
#include "TChain.h"
#include "TLorentzVector.h"
#include "generateHistogram.h"

#include "TMVA/Config.h"
#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"

class MVATree {

public:


  MVATree( generateHistogram::AnalysisType type );

  float pTV;
  float MET;
  float pTB1;
  float pTB2;
  float mBB;
  float dRBB;
  float dPhiVBB;
  float dPhiLBmin;
  float mTW;
  float Mtop;
  float dYWH;

  float pTJ3;
  float mBBJ;
  float BDT;

  TMVA::Reader* reader_2jet_even;
  TMVA::Reader* reader_2jet_odd;
  TMVA::Reader* reader_3jet_even;
  TMVA::Reader* reader_3jet_odd;

  MVATree();
  ~MVATree();


private:

  TMVA::Reader* InitReader( const generateHistogram::AnalysisType type, const generateHistogram::AnalysisRegion reg, const TString Name, const TString WeightFile );

};

#endif
