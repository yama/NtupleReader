#ifndef histSvc_H
#define histSvc_H

#include <string>
#include <vector>
#include <map>

#include "TString.h"
#include "TH1.h"
#include "TH2.h"
#include "TChain.h"
#include "TLorentzVector.h"
#include "generateHistogram.h"

class histSvc {

public:

  void BookFillHist( TString varName, bool isEl,bool isCBA, int nBins, float xmin, float xmax, float value, float weight);
  void BookFillHist( TString varName, bool isEl,bool isCBA, int nBinsX, float xmin, float xmax, float xvalue, int nBinsY, float ymin, float ymax, float yvalue, float weight);
  void SetAnalysisType( generateHistogram::AnalysisType type );
  void SetProcess( TString DSID, generateHistogram::Process process );
  void SetEventFlav( int flav_1, int flav_2 );
  void SetNjets( int njets );
  void SetNFjets( int njets );
  void SetNtags( int ntags );
  void SetDescription( TString description );
  void SetpTV( float ptv );
  void SetFlavor (int j1Flav,int j2Flav);
  void FillCutflow( int bin, float weight, TString name);
  void WriteHists();

  histSvc(TString outputPath);
  ~histSvc();

private:

  TString GetHistoName( TString varName , bool isEl, bool isCBA);
  TString TruthFlavLabel();
  TString m_outputPath;
  TString m_DSID;
  int m_flav_1, m_flav_2;
  int m_njets;
  int m_nfjets;
  int m_ntags;
  float m_ptv;
  TString m_description;
  TString m_flavor;
  TH1F* m_cutflow_Unweighted = new TH1F("cutflow_Unweighted","cutflow_Unweighted",10,-0.5,9.5);
  TH1F* m_cutflow_EventWeight = new TH1F("cutflow_EventWeight","cutflow_EventWeight",10,-0.5,9.5);
  TH1F* m_cutflow_FullWeight = new TH1F("cutflow_FullWeight","cutflow_FullWeight",10,-0.5,9.5);

  generateHistogram::AnalysisType m_type;
  generateHistogram::Process m_process;
  std::vector<TH1F*> m_histograms;
  std::vector<TH2F*> m_histograms_2d;
  std::map<TString,int> m_histoMap;

};

#endif
