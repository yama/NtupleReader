#/bin/bash

startFromNumber=0

jobNumber=$startFromNumber
let lastJobNumber=$jobNumber+5

workdir=$PWD

outdir=$workdir"/output/batchSubmit-$(date +"%y-%m-%d"_%H-%M-%S)"

mkdir -p $outdir

while ( test $jobNumber -lt $lastJobNumber); do



    mkdir -p $outdir"/submitBatch"

    cd  $outdir

    filename=$(echo 'submitJob-'$jobNumber'.sh')

    touch $outdir"/submitBatch/"$filename

    echo "#!/bin/sh" >> "submitBatch/"$filename
    echo "" >> "submitBatch/"$filename
    echo "unset DISPLAY" >> "submitBatch/"$filename
    echo "echo Runnin on host \`hostname\`" >> "submitBatch/"$filename
    echo "cd" $workdir>> "submitBatch/"$filename
    echo "source setup.sh;">> "submitBatch/"$filename
    echo "./NtupleToHist" $jobNumber \"$outdir\" >> "submitBatch/"$filename


    chmod a+x submitBatch/$filename

     bsub -q 1nd "submitBatch/"$filename
  
  #  qsub -N $(echo "run-"$jobNumber) -q medium "submitBatch/"$filename

#    qsub -N $(echo "run-"$jobNumber) -q short $PWD/submitBatch/$filename

#    source $PWD/submitBatch/$filename

    let jobNumber=$jobNumber+1

    cd $workdir



done

