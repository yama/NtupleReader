#!/usr/bin/env python2

import sys,os
import time
import math
from ROOT import *

gROOT.LoadMacro("AtlasStyle.C")
gROOT.LoadMacro("AtlasUtils.C")
SetAtlasStyle()
gROOT.SetBatch(1)
gStyle.SetPalette(1)
gStyle.SetPadRightMargin(0.15);

gROOT.ProcessLine(".x $ROOTCOREDIR/scripts/load_packages.C")
print " "
print "I AM READY TO GO"
print " "

#######################################################################################################################################
#######################################################################################################################################
CDIfile="CDIfiles/2016-20_7-13TeV-MC15-CDI-2017-06-07_v2.root"


jetAlgo="AntiKt4EMTopoJets"

# the [] contian the list of EV, the last one is about the extrapolation
flavours=[
    ["B"    ,  5 ],
    ["C"    ,  4 ],
    ["Light",  0 ],
    ["T"    , 15 ],
]
wps=["60","70","77","85"]

    
########################################################################################################################################
########################################################################################################################################

ofile=TFile("outMaps.root","RECREATE")
fileR=TFile(CDIfile)
for wp in wps:
    print "-----------------------------------------------------------------------------------------------"
    print "  WP: "+wp
    for flav in flavours:
        f0=flav[0]
        print " "
        print " --->  FLAVOUR: "+f0
        Dir=fileR.Get("MV2c10/"+jetAlgo+"/FixedCutBEff_"+wp+"/"+f0)
        contEff =Dir.Get("default_Eff")
        ref=[]
        hadMap=[]
        theMap=Dir.Get("MChadronisation_ref")
        for obj in theMap:
            theName=str(theMap.GetValue(obj))
            hadMap.append( [ str( obj ) , theName ] )
            print "PS string: "+str( obj )+ " is connected to map: "+str( theMap.GetValue(obj) )
        for obj in hadMap:
            tmpCont=Dir.Get( obj[1].replace("CalibrationBinned_","") )
            histo=tmpCont.GetValue("result")
            ofile.WriteObject(histo,wp+"_"+f0+"_"+obj[0])
            
ofile.Close()
fileR.Close()
sys.exit()
