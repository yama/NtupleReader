# Directories
OBJ=obj
SRC=src
INC=inc

# Setting ROOT variables                                                                                           
ROOTCFLAGS := $(shell root-config --cflags)
ROOTLIBS := $(shell root-config --libs)

LIBS         += -lTMVA

# Setting compiler flags
CXX = g++ -std=c++11 -Wall -Wformat=0
CXXFLAGS = $(ROOTCFLAGS) -I$(INC)

LD = g++
UNAME_OS := $(shell lsb_release -si)
ifeq ($(UNAME_OS),Ubuntu)
	LDFLAGS	= "-Wl,--no-as-needed" $(ROOTLIBS) $(LIBS) -L$(OBJ) # ubuntu
else 
	LDFLAGS	= $(ROOTLIBS) $(LIBS) -L$(INC)
endif

# List of sources and objects
sources=$(shell find $(SRC) -type f -name \*.cxx)
objects=$(sources:$(SRC)/%.cxx=$(OBJ)/%.o)

# Default target
default: NtupleToHist

# Generic rule for objects
$(OBJ)/%.o: $(SRC)/%.cxx $(INC)/%.h 
	${CXX} ${CXXFLAGS} -c $< -o $@

# Rule for main object
$(OBJ)/makeHistograms.o: $(SRC)/makeHistograms.cxx
	${CXX} ${CXXFLAGS} -c $< -o $@

# Rule for main program
NtupleToHist: $(objects)
	@echo "objects: $(objects)"
	$(LD) $(LDFLAGS) $^ -o $@

clean:
	rm -rf $(OBJ)/*.o NtupleToHist


